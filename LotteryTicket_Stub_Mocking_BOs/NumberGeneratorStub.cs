﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lottery_Stub_Mocking_BOs_CS
{
    public  class NumberGeneratorStub : INumberGenerator
    {
        private int _number = -1;

        public int Generate( int limit )
        {
            _number += 1;
            return _number;
        }
    }
}
