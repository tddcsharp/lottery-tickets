﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lottery_Stub_Mocking_BOs_CS
{
    public interface INumberGenerator
    {
        int Generate(int limit);
    }
}
