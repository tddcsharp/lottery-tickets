﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lottery_Stub_Mocking_BOs_CS
{
    public  class MyRandomNumberGenerator : INumberGenerator
    {
        private Random rand = new Random();

        public  int Generate(int limit)
        {
            return rand.Next(limit);
        }
    }
}
