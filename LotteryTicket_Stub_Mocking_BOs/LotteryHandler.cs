﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lottery_Stub_Mocking_BOs_CS
{
    public class LotteryHandler
    {
        private INumberGenerator generator;
        private const int LOTTERY_SIZE = 5;
        public const int HIGHEST_NUMBER = 99;

        public LotteryHandler( INumberGenerator generator )
        {
            this.generator = generator;
        }

        public HashSet<int> GenerateRandomSet()
        {
            HashSet<int> numbers = new HashSet<int>();

            while(numbers.Count < LOTTERY_SIZE)
            {
                numbers.Add(generator.Generate(HIGHEST_NUMBER) + 1);
            }

            return numbers;
        }

        public String Format( HashSet<int> numbers )
        {
            int count = 0;
            StringBuilder sb = new StringBuilder();

            foreach( int number in numbers )
            {
                sb.Append(number);
                count += 1;
                if( count < numbers.Count )
                    sb.Append(" - ");
            }
            return sb.ToString();
        }
    }
}
