﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NMock;


namespace Lottery_Stub_Mocking_BOs_CS
{
    [TestFixture]
    class LotteryTest
    {
        private LotteryHandler lotto;
        private INumberGenerator generator;

        [SetUp]
        public void setup()
        {
            INumberGenerator generator = new NumberGeneratorStub();
            lotto = new LotteryHandler(generator);
        }

        [Test]
        public void testGenerateRandomSetReturnsLotteryNumbers()
        {
            HashSet<int> numbers = lotto.GenerateRandomSet();

            String expected = "1 - 2 - 3 - 4 - 5";

            Assert.AreEqual(expected, lotto.Format(numbers));
        }

        [Test]
        public void testStubGenerateRandomSet()
        {
            Console.WriteLine("StubGenerateRandomSet");
            NumberGeneratorStub ngs = new NumberGeneratorStub();
            LotteryHandler instance = new LotteryHandler(ngs);
            HashSet<int> expResult = new HashSet<int>{ 1, 2, 3, 4, 5 };
        
            HashSet<int> result = instance.GenerateRandomSet();
            Assert.That(result.SetEquals(expResult));
        }

        [Test]
        public void testStubGenerateRandomSetWithMocks()
        {
            Console.WriteLine("StubGenerateRandomSetWithMocks");
            HashSet<int> expResult = new HashSet<int> { 1, 2, 3, 4, 5 };

            MockFactory factory = new MockFactory();

            //create a Mock<>
            Mock<INumberGenerator> mockGenerator = factory.CreateMock<INumberGenerator>();

            //Assert that the mock is not null
            Assert.IsNotNull(mockGenerator, "mock was null");

            //Assert that the mock is not of the interface type
            Assert.IsFalse(typeof(INumberGenerator).IsInstanceOfType(mockGenerator));
            generator = mockGenerator.MockObject;

            lotto = new LotteryHandler(generator);

            mockGenerator.Expects.One.MethodWith(m => m.Generate(LotteryHandler.HIGHEST_NUMBER)).WillReturn(0);
            mockGenerator.Expects.One.MethodWith(m => m.Generate(LotteryHandler.HIGHEST_NUMBER)).WillReturn(1);
            mockGenerator.Expects.One.MethodWith(m => m.Generate(LotteryHandler.HIGHEST_NUMBER)).WillReturn(2);
            mockGenerator.Expects.One.MethodWith(m => m.Generate(LotteryHandler.HIGHEST_NUMBER)).WillReturn(3);
            mockGenerator.Expects.One.MethodWith(m => m.Generate(LotteryHandler.HIGHEST_NUMBER)).WillReturn(4);

            HashSet<int> result = lotto.GenerateRandomSet();

            factory.VerifyAllExpectationsHaveBeenMet();
            //mockGenerator.AssertAll();
            //Assert.That(result.SetEquals(expResult));
        }

    }
}
