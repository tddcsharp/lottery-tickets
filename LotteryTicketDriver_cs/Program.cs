﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lottery_Stub_Mocking_BOs_CS;

namespace LotteryDriverCS
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberGeneratorStub gen = new NumberGeneratorStub();

            LotteryHandler lotto = new LotteryHandler(gen);

            HashSet<int> hs = lotto.GenerateRandomSet();

            String numbers = lotto.Format(hs);

            Console.WriteLine(numbers);

            if (numbers.Equals("1 - 2 - 3 - 4 - 5"))
                Console.WriteLine("They are equal");
        }
    }
}
